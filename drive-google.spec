%global debug_package   %{nil}
%global bname           drive
%global commit          bede608f250a9333d55c43396fc5e72827e806fd
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

Name:           %{bname}-google
Version:        0.4.0
Release:        2.%{shortcommit}%{?dist}
Summary:        Pull or push Google Drive files
License:        ASL 2.0
Source0:        https://github.com/odeke-em/drive/archive/%{commit}/%{bname}-%{shortcommit}.tar.gz
BuildRequires:  gcc
BuildRequires:  golang >= 1.9.0
BuildRequires:  git

%description
drive is a program to pull or push Google Drive files.

%prep
#su -c  "rm -fr %{bname}-%{commit}"
%autosetup -n %{bname}-%{commit}

%build
#function gobuild { go build -a -ldflags "-B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d ' \n')" -v -x "$@"; }
#set -m
#export GOPATH="$(pwd)/_build"
#go install ./drive-google

#drive-google
#CGO_ENABLED=0 GOOS=linux %gobuild -o ./bin/%{bname}-google ./cmd/drive/
go build -o ./_build/bin/%{bname}-google ./cmd/drive/
#CGO_ENABLED=0 GOOS=linux go build -o ./bin/%{bname}-server ./cmd/drive/

%install
install -d %{buildroot}%{_bindir}
install -p -m 0755 ./_build/bin/%{bname}-google %{buildroot}%{_bindir}/
#install -p -m 0755 %{bname}-server/%{bname}-server %{buildroot}%{_bindir}/

install -d %{buildroot}%{_datadir}/icons/hicolor/128x128/mimetypes
install -p -m 0644 icons/*.png %{buildroot}%{_datadir}/icons/hicolor/128x128/mimetypes/
install -d %{buildroot}%{_datadir}/icons/hicolor/scalable/mimetypes
install -p -m 0644 icons/*.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/mimetypes/

%files
%defattr(-,root,root,-)
%doc LICENSE README.md
%{_bindir}/*
%{_datadir}/icons/hicolor/*


%changelog
* Wed Jul 20 2022 Boyd Kelly <bkelly@fedoraproject.org> - 0.40-a42ab82
- Add init mod for newer versions of Go

* Sun Apr 26 2020 Vaughan <devel at agrez dot net> - 0.3.9.1-4.52aed62
- Rebuild for f31/32

* Mon May 27 2019 Vaughan <devel at agrez dot net> - 0.3.9.1-3.52aed62
- Rebuild for f30

* Sat Nov 10 2018 Vaughan <devel at agrez dot net> - 0.3.9.1-2.52aed62
- Rebuild for f29
- Clean spec

* Tue Apr 04 2017 Vaughan <devel at agrez dot net> - 0.3.9.1-1.52aed62
- New release
- Update to git commit: 52aed62548f4c408a3042cd2375661ac869d7710

* Sun Jan 15 2017 Vaughan <devel at agrez dot net> - 0.3.9-1.a9f53bc
- New release
- Update to git commit: a9f53bc4cc9d9d2eada836602ad4b4a7902424a6

* Wed Oct 12 2016 Vaughan <devel at agrez dot net> - 0.3.8.1-1.aa79e32
- New release
- Update to git commit: aa79e324a22a2073b68c61783f4015426b7dbe67

* Wed Aug 24 2016 Vaughan <devel at agrez dot net> - 0.3.7-1.bd7e3f5
- New release
- Update to git commit: bd7e3f5ad5a67a27892e47da5c36452bfdfb8513

* Tue May 24 2016 Vaughan <devel at agrez dot net> - 0.3.6-1.d75bc82
- New release
- Update to git commit: d75bc827514eb54378c54452b7bb00b325ffb885

* Mon Mar 14 2016 Vaughan <devel at agrez dot net> - 0.3.5-1.2334708
- Initial package
- Git commit 2334708ccbaff51544634a8b8d93cd08910765cf

